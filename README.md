本次GitCourse的暑期项目主要是完善GitCourse开发者文档，从而帮助开发者全面了解GitCourse的使用流程以及设计理念。😊

本次开发者使用文档的编写用到了GitBook工具，该工具可以快速构建精美的电子书，或者说是优雅美观的开发者文档。

项目运行流程如下(首先需要克隆该项目到自己的本地中)：

1、安装GitBook的命令行工具gitbook-cli，此处使用NPM安装该工具，

`npm install gitbook-cli -g`

2、 查看gitbook-cli的版本信息，并安装GitBook，

`gitbook -V`

3、进入GitBook目录，此时目录中存在构建gitbook所需的文件，比如README.md文件和SUMMARY.md文件，

`cd GitBook`

4、基于GitBook目录中的文件，生成gitbook的静态网页并在本地运行，

`gitbook build && gitbook serve`

5、浏览器中即可访问我们所构建的gitbbok电子书/开发者文档。

访问地址为: http://localhost:4000
