#### 介绍
此处罗列一下GitCourse相关的文档以及仓库、学习教程的访问地址。

##### GitCourse官方界面访问
* http://gitcourse-io.kfcoding.com/
![](https://git-course.oss-cn-shanghai.aliyuncs.com/DeveloperDocumentation/imagesForReadme/webSite.png)

##### GitCourse项目的GitHub仓库位置
* https://github.com/kfcoding/gitcourse
![](https://git-course.oss-cn-shanghai.aliyuncs.com/DeveloperDocumentation/imagesForReadme/Repo.png)

##### GitCourse基础文档学习
* http://kfcoding.com/creation/docs

##### GitCourse使用教程学习
* http://gitcourse.kfcoding.com/#https://code.kfcoding.com/liuchangfreeman/gitcourse-guide.git  

说明，其中该教程需要在xlab实验室基于gitcourse所构建的一站式IT实训平台 http://kfcoding.com 上进行相关操作。
![](https://git-course.oss-cn-shanghai.aliyuncs.com/DeveloperDocumentation/imagesForReadme/gitCoursePractice.png)
